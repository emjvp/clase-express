const express = require(`express`);
const appServer = express();
const path = require("path");
const morgan = require("morgan");
const static = require("static");
const bodyParser = require("body-parser");
//const routes = require("./routes/index");


appServer.set('port', process.env.port||3000)
appServer.set(path.join(__dirname, 'views'))



//Midlewares
appServer.set('view engine','ejs');
appServer.set('appName','suramericaApps')

appServer.use((req,res,next) => {
    console.log(`urlRequerida:${req.url} - métodorequerido:${req.method}`);
    next()
})

appServer.use(morgan('combined'))

appServer.use(bodyParser.urlencoded({extended:true}))
appServer.use(bodyParser.json())

appServer.use(express.static(path.join(__dirname, 'public')));74

appServer.listen(appServer.get('port'),()=>{
    console.log(`listened on port ${appServer.get('port')} + ${appServer.get('appName')}`)
    })





